let angka1 = document.getElementById("angka1");
let angka2 = document.getElementById("angka2");
let hasil = document.getElementById("hasil");


let tambah = document.getElementById("tambah");
tambah.addEventListener("click", function(e) {
    let value = parseInt(angka1.value) + parseInt(angka2.value);
    hasil.textContent = value
});

let kurang = document.getElementById("kurang");
kurang.addEventListener("click", function(e) {
    let value = parseInt(angka1.value) - parseInt(angka2.value);
    hasil.textContent = value
});

let kali = document.getElementById("kali");
kali.addEventListener("click", function(e) {
    let value = parseInt(angka1.value) * parseInt(angka2.value);
    hasil.textContent = value
});

let bagi = document.getElementById("bagi");
bagi.addEventListener("click", function(e) {
    let value = parseInt(angka1.value) / parseInt(angka2.value);
    hasil.textContent = value
});

let hapus = document.getElementById("hapus");

hapus.addEventListener("click", function(e) {
    angka1.value = "";
    angka2.value = "";
    hasil.textContent = "";
})